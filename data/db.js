import mongoose from 'mongoose';

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost/clientes', {useNewUrlParser: true,  useUnifiedTopology: true});

// definir el schema de clientes

const clientesSchema = new mongoose.Schema({
    nombre: String,
    apellido: String,
    tipo: String,
    email: String,
    pedidos: Array
});

const Clientes = mongoose.model('clientes', clientesSchema );

export { Clientes };